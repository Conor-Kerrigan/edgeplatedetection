from plate_detector import GDPR
import json, io, base64, cv2
import numpy as np
from urllib.request import urlopen
from PIL import Image, ImageFilter

def lambda_handler(event, context):
    if 'image_id' in event:
        image_id = event['image_id']
        url = "https://s3-eu-west-1.amazonaws.com/focalagentdata/uploads/" + image_id
        data = bytearray(urlopen(url).read())
        np_img = np.asarray(data, dtype=np.uint8)
        img = cv2.imencode(np_img, -1)
        img_gray = cv2.cvtColor(np_img, cv2.COLOR_BGR2GRAY)
    else:
        data = bytearray(event['img_bytes'])
        np_img = np.asarray(data, dtype=np.uint8)
        img = cv2.imencode(np_img, -1)
        img_gray = cv2.cvtColor(np_img, cv2.COLOR_BGR2GRAY)
    issues = GDPR(img_gray)
    return json.dumps(issues)

