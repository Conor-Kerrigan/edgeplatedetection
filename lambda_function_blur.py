import json, io, base64
from urllib.request import urlopen
from PIL import Image, ImageFilter

def lambda_handler(event, context):
    image_id = event['image_id']
    issues = json.loads(event['issues'])
    if image_id is not None:
        url = "https://s3-eu-west-1.amazonaws.com/focalagentdata/uploads/" + image_id
        data = io.BytesIO(urlopen(url).read())
        image_obj = Image.open(data)
    else:
        data = {}
        data["img"] = event["img_bytes"]
        image_obj = Image.open(event["img_bytes"])
    
    
    for issue in issues:
        coords = issue["coords"].split(" ")
        rel_x = int(float(image_obj.size[0])*float(coords[0]))
        rel_y = int(float(image_obj.size[1])*float(coords[1]))
        rel_w = int(float(image_obj.size[0])*float(coords[2]))
        rel_h = int(float(image_obj.size[1])*float(coords[3]))
        far_left = int(rel_x - (rel_w/2))
        far_right = int(rel_x + (rel_w/2))
        top = int(rel_y - (rel_h/2))
        bottom = int(rel_y + (rel_h/2))
        box = (far_left,top,far_right,bottom)
        gdpr_item = image_obj.crop(box)
        for _ in range(int(20)):
            gdpr_item = gdpr_item.filter(ImageFilter.BLUR)
        image_obj.paste(gdpr_item,box)
    memory_img = io.BytesIO()
    image_obj.save(memory_img, format="JPEG")
    resp = {}
    resp["censored-image"] = image_obj
    return resp

