import cv2, imutils, pytesseract, re, math, os, json, base64, io
import numpy as np
from lambda_function_blur import lambda_handler
from PIL import Image, ImageEnhance, ImageFilter
#from shapely.geometry import Polygon

plate_regex = "([A-Z0-9\-\ ]{4,15})"
digit_regex = ".*[0-9].*"
char_regex = ".*[A-Z].*"
 
class GDPR:
    def __init__(self, image):
        self.resp = detect_gdpr(image)
    
    def get_issues(self):
        return self.resp

def image_resize(image, width = None, height = None, inter = cv2.INTER_AREA):
    # initialize the dimensions of the image to be resized and
    # grab the image size
    dim = None
    (h, w) = image.shape[:2]

    # if both the width and height are None, then return the
    # original image
    if width is None and height is None:
        return image

    # check to see if the width is None
    if width is None:
        # calculate the ratio of the height and construct the
        # dimensions
        r = height / float(h)
        dim = (int(w * r), height)

    # otherwise, the height is None
    else:
        # calculate the ratio of the width and construct the
        # dimensions
        r = width / float(w)
        dim = (width, int(h * r))

    # resize the image
    resized = cv2.resize(image, dim, interpolation = inter)

    # return the resized image
    return resized

def angle_between_points(point1, point2):
    rad = math.atan2(point1[1]-point2[1],point1[0]-point2[0])
    return math.degrees(rad)

def rotate_image(image, angle):
    image_center = tuple(np.array(image.shape[1::-1]) / 2)
    rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
    result = cv2.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR)
    return result


def check_rect_duplicate(rect1, rect2):
    box1 = rect1.split(" ")
    box2 = rect2.split(" ")
    threshold = 0.05
    if float(box2[0])-threshold <= float(box1[0]) <= float(box2[0]) + threshold:
        if float(box2[1])-threshold <= float(box1[1]) <= float(box2[1]) + threshold:
            return True
    return False

def increase_contrast(img):
    pilImg = Image.fromarray(img)
    enhancer = ImageEnhance.Contrast(pilImg)
    pilImg = enhancer.enhance(2)
    return np.array(pilImg)

def enhance_edges(img):
    pilImg = Image.fromarray(img)
    edge_enhance = pilImg.filter(ImageFilter.EDGE_ENHANCE_MORE)
    return np.array(edge_enhance)

def get_rectangle_coords(box):
    pts = box
    rect = np.zeros((4, 2), dtype="float32")
	# the top-left point will have the smallest sum, whereas
	# the bottom-right point will have the largest sum
    s = pts.sum(axis=1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]
    # now, compute the difference between the points, the
    # top-right point will have the smallest difference,
    # whereas the bottom-left will have the largest difference
    diff = np.diff(pts, axis=1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]
    return (rect[0], rect[1], rect[2], rect[3])

def check_list_for_duplicates(box, box_list):
    for comp_box in box_list:
        if check_rect_duplicate(box,comp_box["coords"]):
            return True
    return False

def adjust_box_to_angle(box, angle):
    vals = box.split(" ")
    if angle < 0:
        angle = angle * -1
    if angle > 20:
        vals[3] = str(float(vals[3])*1.8)
    elif angle > 10:
        vals[3] = str(float(vals[3])*1.6)
    return str(vals[0]) + " " + str(vals[1]) + " " + str(vals[2]) + " " + str(vals[3])


def auto_canny(image, sigma=0.33):
	# compute the median of the single channel pixel intensities
	v = np.median(image)
	# apply automatic Canny edge detection using the computed median
	lower = int(max(0, (1.0 - sigma) * v))
	upper = int(min(255, (1.0 + sigma) * v))
	edged = cv2.Canny(image, lower, upper)
	# return the edged image
	return edged

def get_relative_box_string(box, img_res):
    ctrX = (box[0][0] + box[1][0] + box[2][0] + box[3][0])/4
    ctrY = (box[0][1] + box[1][1] + box[2][1] + box[3][1])/4
    bxWidth = ((box[2][0]+box[1][0])/2)-((box[0][0]+box[3][0])/2)
    bxHeight = ((box[2][1]+box[3][1])/2)-((box[0][1]+box[1][1])/2)
    scaledCtrX = float((ctrX/img_res.shape[1]))
    scaledCtrY = float((ctrY/img_res.shape[0]))
    scaledWidth = float((bxWidth/img_res.shape[1]))
    scaledHeight = float((bxHeight/img_res.shape[0]))
    coords_str = str(scaledCtrX) + " " + str(scaledCtrY) + " " + str(scaledWidth) + " " + str(scaledHeight)
    return coords_str

def get_rectangle_contours(gray):
    #gray = enhance_edges(gray)
    gray = increase_contrast(gray)
    gray = cv2.bilateralFilter(gray, 10, 50, 75)
    edges = auto_canny(gray)
    contours = cv2.findContours(edges.copy(),cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
    contours = imutils.grab_contours(contours)
    contours = sorted(contours,key=cv2.contourArea, reverse = True)[:150]
    screenCnt = []
    for c in contours:
        # approximate the contour
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.03 * peri, True)
        # if our approximated contour has four points, then
        # we can assume that we have found our screen
        if len(approx) == 4:
            rect = cv2.minAreaRect(c)
            screenCnt.append([approx,rect])
    return screenCnt, gray


def detect_gdpr(img):
    if img.shape[0] > 2000:
        img_res = image_resize(img, 2000)
    else: 
        img_res = img
    screenCnt, gray = get_rectangle_contours(img_res)
    face_cascade = cv2.CascadeClassifier('cascade.xml')
    issues = []
    found_plates = []
    for sCt in screenCnt:
        box = get_rectangle_coords(cv2.boxPoints(sCt[1]))
        angle_from_horizontal = angle_between_points(box[1],box[0])
        mask = np.zeros(gray.shape,np.uint8)
        new_image = cv2.drawContours(mask,[sCt[0]],0,255,-1,)
        new_image = cv2.bitwise_and(img_res,img_res,mask=mask)
        (x, y) = np.where(mask == 255)
        (topx, topy) = (np.min(x), np.min(y))
        (bottomx, bottomy) = (np.max(x), np.max(y))
        margin = 5
        Cropped = gray[topx-margin:bottomx+margin, topy-margin:bottomy+margin]
        if(Cropped.shape[0] > 0 and Cropped.shape[1] > 0):
            box_str = get_relative_box_string(box, img_res)
            res = {"coords": box_str}
            #cv2.waitKey(0)
            if not check_list_for_duplicates(box_str, issues):
                faces = face_cascade.detectMultiScale(Cropped, 1.1, 4)
                if len(faces) > 0:
                    res["label"] = "1"
                    issues.append(res)
                    continue
                #Cropped = enhance_edges(Cropped)
                Cropped = rotate_image(Cropped, angle_from_horizontal)
                #Cropped = increase_contrast(Cropped)
                #Cropped = cv2.bilateralFilter(Cropped, 20, 15, 15)
                text = pytesseract.image_to_string(Cropped, config='--psm 6')
                text = text.replace(" ", "")
                is_plate = re.search(plate_regex,text)
                match = None
                if is_plate is not None:
                    match = is_plate[0]
                if is_plate is not None and match not in found_plates and float(box_str.split(" ")[2]) > float(box_str.split(" ")[3]):
                    if re.search(char_regex, match) is not None and re.search(digit_regex, match) is not None:
                        res["coords"] = adjust_box_to_angle(res["coords"], angle_from_horizontal)
                        found_plates.append(is_plate[0])
                        res["label"] = "0"
                        issues.append(res)
    return issues
'''           
results = {}
test_dir = "test_images_2"
test_images = os.listdir(test_dir)
correct = 0
total = 0
for image in test_images:
    total+=1
    img = cv2.imread(os.path.join(test_dir,image),cv2.IMREAD_GRAYSCALE)
    results[image] = detect_gdpr(img)
    if len(results[image]) > 0:
        print("Found")
        img = cv2.imread(os.path.join(test_dir,image),cv2.IMREAD_COLOR)
        correct+=1
        request = {}
        imgBytes = io.BytesIO()
        Image.fromarray(img).save(imgBytes, format="JPEG")
        imgBytes.seek(0)
        request["issues"] = json.dumps(results[image])
        request["img_bytes"] = imgBytes
        request["image_id"] = None
        resp = lambda_handler(request,None)
        #imgBytes = io.BytesIO(resp["censored-image"])
        pilImage = resp["censored-image"]
        pilImage = cv2.cvtColor(np.array(pilImage), cv2.COLOR_BGR2RGB)
        pilImage = Image.fromarray(pilImage)
        pilImage.save("output2/"+image)
    else:
        print("Not found")
    print(image)
    print(str((correct/total)*100))

print(str((correct/total)*100))
print("done")
'''