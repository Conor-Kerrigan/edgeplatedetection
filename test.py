from lambda_function_gdpr import lambda_handler
import base64

event = {}
with open("test_images_2/azure1.jpg", "rb") as img:
    bytes_str = img.read()
event["img_bytes"] = bytes_str

lambda_handler(event, None)